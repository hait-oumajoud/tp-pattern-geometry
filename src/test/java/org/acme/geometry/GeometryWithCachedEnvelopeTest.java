package org.acme.geometry;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;

public class GeometryWithCachedEnvelopeTest extends TestCase {
    public static final double EPSILON = 1.0e-15;

    @Test
    public void testConstructor() {
        Geometry a = new Point(new Coordinate(1.0, 1.0));
        GeometryWithCachedEnvelope g = new GeometryWithCachedEnvelope(a);
        Envelope e = g.getEnvelope();
        Envelope b = g.getEnvelope();
        Assert.assertSame(e, b);
    }
    @Test
    public void testTranslate(){
        Geometry g = new Point(new Coordinate(1.0, 1.0));
        GeometryWithCachedEnvelope gc = new GeometryWithCachedEnvelope(g);
        gc.translate(1.0, 1.0);
        Assert.assertEquals(2.0, g.getEnvelope().getXmax(), EPSILON);
    }
    @Test
    public void testGetType(){
        Geometry g = new Point(new Coordinate(1.0, 1.0));
        GeometryWithCachedEnvelope gc = new GeometryWithCachedEnvelope(g);
        Assert.assertEquals("Point", gc.getType());
    }
    @Test
    public void testClone(){
        Geometry g = new Point(new Coordinate(1.0, 1.0));
        GeometryWithCachedEnvelope gc1 = new GeometryWithCachedEnvelope(g);
        GeometryWithCachedEnvelope gc2 = gc1.clone();
        gc1.translate(0.1, 0.1);
        Assert.assertEquals(1.0, ((Point)gc2.getOriginal()).getCoordinate().getX(), EPSILON);
    }
    @Test
    public void testAccept() throws UnsupportedEncodingException {
        Geometry g = new Point(new Coordinate(1.0, 1.0));
        GeometryWithCachedEnvelope gc = new GeometryWithCachedEnvelope(g);
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        PrintStream out = new PrintStream(os);
        LogGeometryVisitor logVisitor = new LogGeometryVisitor(out);
        new LogGeometryVisitor();
        WktVisitor wktVisitor = new WktVisitor();
        gc.accept(logVisitor);
        gc.accept(wktVisitor);
        Assert.assertEquals("Je suis un point avec x=1.0 et y=1.0", os.toString("UTF8").trim());
        Assert.assertEquals("POINT (1.0 1.0)\n", wktVisitor.getResult());
        Assert.assertEquals(false, gc.isEmpty());
    }

}