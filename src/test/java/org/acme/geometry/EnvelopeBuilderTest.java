package org.acme.geometry;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

public class EnvelopeBuilderTest extends TestCase {
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testDefaultConstructor() {
        EnvelopeBuilder eb = new EnvelopeBuilder();
        eb.insert(new Coordinate(0.0,1.0));
        eb.insert(new Coordinate(2.0,0.0));
        eb.insert(new Coordinate(1.0,3.0));
        Envelope e = eb.build();
        Assert.assertEquals(0.0 , e.getXmin(), EPSILON);
        Assert.assertEquals(0.0 , e.getYmin(), EPSILON);
        Assert.assertEquals(2.0 , e.getXmax(), EPSILON);
        Assert.assertEquals(3.0 , e.getYmax(), EPSILON);
        Assert.assertEquals(false , e.isEmpty());
    }

}