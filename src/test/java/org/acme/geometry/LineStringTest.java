package org.acme.geometry;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import java.util.ArrayList;
import java.util.List;

public class LineStringTest extends TestCase {
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testDefaultConstructor() {
        LineString l = new LineString();
        Assert.assertEquals(0,l.getNumPoints());
    }
    @Test
    public void testConstructor() {
        Coordinate c1 = new Coordinate(1.0,2.0);
        Coordinate c2 = new Coordinate(3.0,3.2);
        Point p1 = new Point(c1);
        Point p2 = new Point(c2);
        ArrayList<Point> lp = new ArrayList<Point>();
        lp.add(p1);
        lp.add(p2);
        LineString ls = new LineString(lp);
        Assert.assertEquals(1.0 , ls.getPointsN(0).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(2.0 , ls.getPointsN(0).getCoordinate().getY(), EPSILON);
        Assert.assertEquals(3.0 , ls.getPointsN(1).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(3.2 , ls.getPointsN(1).getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testGetType() {
        LineString l = new LineString();
        Assert.assertEquals("LineString",l.getType());
    }
    @Test
    public void testIsEmpty() {
        ArrayList<Point> lp = new ArrayList<Point>();
        LineString ls = new LineString(lp);
        Assert.assertEquals(true , ls.isEmpty());

    }
    @Test
    public void testTranslate() {
        Coordinate c1 = new Coordinate(1.0,2.0);
        Coordinate c2 = new Coordinate(3.0,4.0);
        Point p1 = new Point(c1);
        Point p2 = new Point(c2);
        ArrayList<Point> lp = new ArrayList<Point>();
        lp.add(p1);
        lp.add(p2);
        LineString ls = new LineString(lp);
        ls.translate(0.1, 0.1);
        Assert.assertEquals(1.1 , ls.getPointsN(0).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(2.1 , ls.getPointsN(0).getCoordinate().getY(), EPSILON);
        Assert.assertEquals(3.1 , ls.getPointsN(1).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(4.1 , ls.getPointsN(1).getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testClone(){
        Coordinate c1 = new Coordinate(1.0,2.0);
        Coordinate c2 = new Coordinate(3.0,4.0);
        Point p1 = new Point(c1);
        Point p2 = new Point(c2);
        p1.translate(0.1,0.2);
        p2.translate(0.3,0.4);
        ArrayList<Point> lp = new ArrayList<Point>();
        lp.add(p1);
        lp.add(p2);
        LineString ls1 = new LineString(lp);
        LineString ls2 = ls1.clone();
        Assert.assertEquals(ls2.getPointsN(0).getCoordinate().getX() , ls1.getPointsN(0).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(ls2.getPointsN(0).getCoordinate().getY(), ls1.getPointsN(0).getCoordinate().getY(), EPSILON);
        Assert.assertEquals(ls2.getPointsN(1).getCoordinate().getX(), ls1.getPointsN(1).getCoordinate().getX(), EPSILON);
        Assert.assertEquals(ls2.getPointsN(1).getCoordinate().getY() , ls1.getPointsN(1).getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testGetEnvelope() {
        List<Point> lp = new ArrayList<>();
        Point p1 = new Point(new Coordinate(1.0, 2.0));
        Point p2 = new Point(new Coordinate(2.0, 1.0));
        lp.add(p1);
        lp.add(p2);
        LineString ls = new LineString(lp);
        Envelope e = ls.getEnvelope();
        Assert.assertNotNull(e);
        Assert.assertEquals(1.0 , e.getXmin(), EPSILON);
        Assert.assertEquals(1.0 , e.getYmin(), EPSILON);
        Assert.assertEquals(2.0 , e.getXmax(), EPSILON);
        Assert.assertEquals(2.0 , e.getYmax(), EPSILON);
    }
    @Test
    public void testAccept() {
        LogGeometryVisitor visitor = new LogGeometryVisitor();
        List<Point> lp = new ArrayList<>();
        Geometry p1 = new Point(new Coordinate(1.0,1.0));
        Geometry p2 = new Point(new Coordinate(1.0,1.0));
        lp.add((Point) p1);
        lp.add((Point) p2);
        LineString ls = new LineString(lp);
        ls.accept(visitor);
    }
}