package org.acme.geometry;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class WktWriterTest extends TestCase {
    @Test
    public void testWritePointEmpty() {
        Point p = new Point();
        WktWriter writer = new WktWriter();
        assertEquals("POINT EMPTY", writer.write(p));
    }
    @Test
    public void testWritePointNotEmpty() {
        Geometry p = new Point(new Coordinate(3.0,4.0));
        WktWriter writer = new WktWriter();
        assertEquals("POINT(3.0 4.0)", writer.write(p));
    }
    @Test
    public void testWriteLineStringEmpty() {
        LineString ls = new LineString();
        WktWriter writer = new WktWriter();
        Assert.assertEquals("LINESTRING EMPTY",writer.write(ls));
    }
    @Test
    public void testWriteLineStringNotEmpty() {
        Coordinate c1 = new Coordinate(1.0,2.0);
        Coordinate c2 = new Coordinate(3.0,3.2);
        Point p1 = new Point(c1);
        Point p2 = new Point(c2);
        ArrayList<Point> lp = new ArrayList<Point>();
        lp.add(p1);
        lp.add(p2);
        LineString ls = new LineString(lp);
        WktWriter writer = new WktWriter();
        Assert.assertEquals("LINESTRING(1.0 2.0,3.0 3.2)",writer.write(ls));
    }
}