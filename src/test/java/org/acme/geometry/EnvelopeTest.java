package org.acme.geometry;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;


public class EnvelopeTest extends TestCase {
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testDefaultConstructor() {
        Envelope e = new Envelope();
        Assert.assertTrue(Double.isNaN(e.getXmin()));
        Assert.assertTrue(Double.isNaN(e.getYmin()));
        Assert.assertTrue(Double.isNaN(e.getXmax()));
        Assert.assertTrue(Double.isNaN(e.getYmax()));
        Assert.assertTrue(e.isEmpty());
    }
    @Test
    public void testConstructor() {
        EnvelopeBuilder eb=new EnvelopeBuilder();
        eb.insert(new Coordinate(1.0,2.0));
        Envelope e = eb.build();
        Assert.assertEquals(1.0 , e.getXmin(), EPSILON);
        Assert.assertEquals(2.0 , e.getYmin(), EPSILON);
        Assert.assertEquals(1.0 , e.getXmax(), EPSILON);
        Assert.assertEquals(2.0 , e.getYmax(), EPSILON);
        Assert.assertEquals(false , e.isEmpty());
    }
}