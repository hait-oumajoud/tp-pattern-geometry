package org.acme.geometry;

import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;

import java.util.ArrayList;

public class AbstractGeometryTest extends TestCase {
    @Test
    public void testAsText() {

        Point p = new Point(new Coordinate(1.0,1.0));
        Assert.assertEquals("POINT (1.0 1.0)\n", p.asText() );

        Coordinate c1 = new Coordinate(1.0,2.0);
        Coordinate c2 = new Coordinate(3.0,3.2);
        Point p1 = new Point(c1);
        Point p2 = new Point(c2);
        ArrayList<Point> lp = new ArrayList<Point>();
        lp.add(p1);
        lp.add(p2);
        LineString ls = new LineString(lp);
        Assert.assertEquals("LINESTRING (1.0 2.0,3.0 3.2)\n", ls.asText() );
    }
}