package org.acme.geometry;
import junit.framework.TestCase;
import org.junit.Assert;
import org.junit.Test;
import java.io.UnsupportedEncodingException;

public class PointTest extends TestCase {
    public static final double EPSILON = 1.0e-15;
    @Test
    public void testDefaultConstructor() {
        Point p = new Point();
        Assert.assertEquals(Double.NaN,p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(Double.NaN,p.getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testConstructor() {
        Coordinate c = new Coordinate(1.0,3.2);
        Point p = new Point(c);
        Assert.assertEquals(1.0 , p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(3.2, p.getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testGetType() {
        Point p = new Point();
        Assert.assertEquals("Point",p.getType());
    }
    @Test
    public void testIsEmpty() {
        Coordinate c = new Coordinate();
        Point p = new Point(c);
        Assert.assertEquals(true, p.isEmpty());
    }
    @Test
    public void testTranslate() {
        Point p = new Point(new Coordinate(1.0,1.0));
        p.translate(0.1,0.1);
        Assert.assertEquals(1.1, p.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(1.1, p.getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testClone(){
        Point p1 = new Point(new Coordinate(1.0,1.0));
        Point p2 = p1.clone();
        Assert.assertEquals(p1.getCoordinate().getX(), p2.getCoordinate().getX(), EPSILON);
        Assert.assertEquals(p1.getCoordinate().getY(), p2.getCoordinate().getY(), EPSILON);
    }
    @Test
    public void testGetEnvelope() {
        Point p = new Point(new Coordinate(1.0, 1.0));
        Envelope e = p.getEnvelope();
        Assert.assertNotNull(e);
        Assert.assertEquals(1.0 , e.getXmin(), EPSILON);
        Assert.assertEquals(1.0 , e.getYmin(), EPSILON);
        Assert.assertEquals(1.0 , e.getXmax(), EPSILON);
        Assert.assertEquals(1.0 , e.getYmax(), EPSILON);
    }
    @Test
    public void testAccept() {
        LogGeometryVisitor visitor = new LogGeometryVisitor();
        Geometry geometry = new Point(new Coordinate(1.0,1.0));
        geometry.accept(visitor);
    }
}
