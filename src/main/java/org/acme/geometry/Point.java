package org.acme.geometry;

public class Point extends AbstractGeometry{
    private Coordinate coordinate;
    public Point(){
        this.coordinate = new Coordinate();
    }
    public Point(Coordinate coordinate){
        this.coordinate = coordinate ;
    }
    public Coordinate getCoordinate() {
        return coordinate;
    }
    @Override
    public String getType() {
        return "Point";
    }
    @Override
    public Boolean isEmpty() {
        return coordinate.isEmpty();
    }
    @Override
    public void translate(Double dx, Double dy) {
        Coordinate p = new Coordinate(this.coordinate.getX() + dx, this.coordinate.getY() + dy);
        this.coordinate=p;
    }
    @Override
    public Point clone(){
        Coordinate c =this.coordinate;
        return new Point(c);
    }
    @Override
    public Envelope getEnvelope() {
        EnvelopeBuilder eb = new EnvelopeBuilder();
        eb.insert(this.coordinate);
        return eb.build();
    }
    @Override
    public void accept(GeometryVisitor visitor) {
        visitor.visit(this);
    }
}
