package org.acme.geometry;

public class GeometryWithCachedEnvelope implements Geometry {
    private Geometry original;

    private Envelope cachedEnvelope;

    public GeometryWithCachedEnvelope(Geometry original) {
        this.original = original;
        this.cachedEnvelope = new Envelope();
    }
    @Override
    public String getType() {
        return original.getType();
    }

    @Override
    public Boolean isEmpty() {
        return original.isEmpty();
    }

    @Override
    public void translate(Double dx, Double dy) {
        original.translate(dx, dy);
    }

    @Override
    public GeometryWithCachedEnvelope clone() {
        return new GeometryWithCachedEnvelope(original.clone());
    }

    @Override
    public Envelope getEnvelope() {
        return this.cachedEnvelope;
    }
    public Geometry getOriginal() {
        return this.original;
    }

    @Override
    public void accept(GeometryVisitor visitor) {
        original.accept(visitor);
    }
}
