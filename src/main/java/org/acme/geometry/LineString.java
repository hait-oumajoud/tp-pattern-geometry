package org.acme.geometry;
import java.util.ArrayList;
import java.util.List;

public class LineString extends AbstractGeometry{
    private List<Point> points;
    public LineString(){
        this.points = new ArrayList<>();
    }
    public LineString(List<Point> points) {
        this.points = points;
    }
    int getNumPoints(){
        return points.size();
    }
    public Point getPointsN(int n) {
        return points.get(n);
    }
    @Override
    public String getType() {
        return "LineString";
    }
    @Override
    public Boolean isEmpty() {
        return points.isEmpty();
    }
    @Override
    public void translate(Double dx, Double dy) {
        ArrayList pointsTranslated = new ArrayList<>();
        for (Point p : points){
            p.translate(dx,dy);
            pointsTranslated.add(p);
        }
        this.points = pointsTranslated;
    }
    @Override
    public LineString clone(){
        List ls = this.points;
        return new LineString(ls);
    }
    @Override
    public Envelope getEnvelope() {
        EnvelopeBuilder eb = new EnvelopeBuilder();
        for (Point p : points){
            eb.insert(p.getCoordinate());
        }
        return eb.build();
    }
    @Override
    public void accept(GeometryVisitor visitor) {
        visitor.visit(this);
    }
}
