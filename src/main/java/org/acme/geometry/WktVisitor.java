package org.acme.geometry;

public class WktVisitor implements GeometryVisitor{
    private StringBuilder buffer = new StringBuilder("");

    @Override
    public void visit(Point point) {
        buffer.append("POINT (");
        buffer.append(point.getCoordinate().getX());
        buffer.append(" ");
        buffer.append(point.getCoordinate().getY());
        buffer.append(")\n");
    }

    @Override
    public void visit(LineString lineString) {
        buffer.append("LINESTRING (");
        for (int i = 0; i<lineString.getNumPoints(); i++){
            buffer.append(lineString.getPointsN(i).getCoordinate().getX() + " ");
            buffer.append(lineString.getPointsN(i).getCoordinate().getY());
            if (i< lineString.getNumPoints()-1){
                buffer.append(",");
            }
        }
        buffer.append(")\n");
    }
    public String getResult(){
        return buffer.toString();
    }
}
