package org.acme.geometry;

import java.util.Locale;

public class WktWriter {
    public String write (Geometry geometry) throws RuntimeException{
        String typeUpper = geometry.getType().toUpperCase(Locale.getDefault());
        if (geometry instanceof Point){
            Point p = (Point)geometry;
            if (p.isEmpty()){
                return typeUpper + " EMPTY";
            } else {
                return typeUpper + "(" + p.getCoordinate().getX() + " " + p.getCoordinate().getY() + ")";
            }
        }else if ( geometry instanceof LineString ){
            LineString ls= (LineString)geometry;
            if (ls.isEmpty()){
                return typeUpper + " EMPTY";
            } else {
                typeUpper +="(";
                for (int i=0; i< ls.getNumPoints(); i++){
                    typeUpper+= ls.getPointsN(i).getCoordinate().getX() + " " + ls.getPointsN(i).getCoordinate().getY() + ",";
                } return typeUpper.substring(0, typeUpper.length()-1) +")";
            }
        }else{
            throw new RuntimeException("geometry typeUpper not supported");
        }
    }
}
