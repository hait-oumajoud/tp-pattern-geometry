package org.acme.geometry;
import org.jetbrains.annotations.NotNull;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EnvelopeBuilder implements GeometryVisitor {
    List<Double> xvals;
    List<Double> yvals;
    public EnvelopeBuilder() {
        this.xvals = new ArrayList<Double>();
        this.yvals = new ArrayList<Double>();
    }
    public void insert(@NotNull Coordinate coordinate){
        this.xvals.add(coordinate.getX());
        this.yvals.add(coordinate.getY());
    }
    public Envelope build(){
        Coordinate bottomLeft = new Coordinate(Collections.min(xvals), Collections.min(yvals));
        Coordinate topRight = new Coordinate(Collections.max(xvals), Collections.max(yvals));
        return new Envelope(bottomLeft, topRight);
    }

    @Override
    public void visit(Point point) {
        this.insert(point.getCoordinate());
    }
    @Override
    public void visit(LineString lineString) {
        for (int i=0; i<lineString.getNumPoints(); i++){
            this.insert(lineString.getPointsN(i).getCoordinate());
        }
    }
}
