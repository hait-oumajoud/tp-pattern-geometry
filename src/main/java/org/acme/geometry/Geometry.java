package org.acme.geometry;

public interface Geometry {
    String getType();
    Boolean isEmpty();
    void translate(Double dx, Double dy);
    public Geometry clone();
    public Envelope getEnvelope();
    void accept(GeometryVisitor visitor);
}
